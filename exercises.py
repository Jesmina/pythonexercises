#Ex.1 Function to check if a number is even or not
def is_even(number):
    if(number%2==0):
        return True
    else:
        return False

#Ex.2 Function to check if a number is palindrome
def palindrome(number):
    temp=number
    reverse_number=0
    while(number>0):
        digit= number%10
        reverse_number= reverse_number*10 + digit
        number = number//10
    if(temp==reverse_number):
        print(f"{temp} "+ " is palindrome")
    else:
        print(f"{temp}"+ " is not")

#Ex.3 Function to check if a number is power of 2
def power_two(number):
    if(number==0):
        return False
    while(number != 1):
        if(number%2!=0):
            return False
        number=number//2
    return True

#Ex.4 Convert a string to uppercase
def upper_case(str_check):
    nr_upper=0
    for i in str_check[:4]:
        if i.upper() == i:
            nr_upper += 1
    if nr_upper >=2:
        return str_check.upper()
    return str_check

#Ex.7 Create a class Person and an inherit class Student
class Person:

    def __init__(self , first_name, last_name):
        self.first_name= first_name
        self.last_name = last_name
    def get_fullname(self):
        return self.first_name + self.last_name

class Student(Person):
    def __init__(self, first_name, last_name):
        super().__init__(first_name,last_name)

    def get_fullname(self):
        return self.first_name + self.last_name + "-st"


#Ex.1 Even Number
num1=int(input("Enter a number to check if it is even or not:"))
print(is_even(num1))

#Ex.2 Palindrome Number
num2=int(input("Enter a number to check if it is a palindrome:"))
palindrome(num2)

#Ex.3 Power of two
num3=int(input("Enter a number:"))
print(power_two(num3))

#Ex.4 Convert to uppercase
str_check = input("Enter a string:")
print(upper_case(str_check))

#Ex.5 Given a list iterate it and display numbers which are divisible by 5 and if you
#find number greater than 150 stop the loop iteration

list1 = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]

for i in list1:
    if(i>150):
        break
    if(i%5==0):
        print(i)

#Ex.6 Write a function that takes a list of words and groups the words by their length using a dictionary
def myFunc(e):
    return len(e)

def create_dictionary(list2):
    d = {}
    for str1 in list2:
        words = len(str1)
        if words in d:
            d[words].add(str1)
        else:
            d[words]={str1}
    return d


list2= ['Lorem', 'ele', 'sit', 'incididunt', 'a', 'su', 'dom', 'ouch', 'ipsum']

list2.sort(key=myFunc)

print(create_dictionary(list2))



#Ex.7 Class Person and Student

p1= Person("Jesmina", " Kraja")
print(p1.get_fullname())
s1= Student("Klotilda", " Kraja")
print(s1.get_fullname())